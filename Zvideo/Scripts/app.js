﻿var Zvideo = angular.module("ZVideo", ['ngSanitize', 'ngResource']);

 

Zvideo.controller("Index", ['$scope', '$sce' , function($scope, $sce) {


    $scope.articles = [];
    $scope.rssFeed = 'http://www.xvideos.com/rss/rss.xml';
    $scope.originalRssFeed = 'http://www.xvideos.com/rss/rss.xml';

    $scope.existingArticles = function() {
        return _.find($scope.articles, function(a) { return !a.cleared }) != null;
    };

    $scope.allAreRead = function() {
        return _.every($scope.articles, function(a) { return a.read; });
    };

    $scope.showOrHideAll = function() {
        var markAsHide = _.every($scope.articles, function(a) { return a.show; });
        _.each($scope.articles, function(el, index, list) { el.show = !markAsHide; });
    };

    $scope.clearCompleted = function() {
        _.each(_.where($scope.articles, { read: true }), function(a) { a.cleared = true; });
    };

    $scope.toggleShow = function(article) {
        article.show = !article.show;
    };

    $scope.markAsRead = function(article) {
        article.read = !article.read;
    };

    $scope.markAll = function() {
        var markAsUnread = $scope.allAreRead();
        _.each($scope.articles, function(el, index, list) { el.read = !markAsUnread; });
    };

    var hostname = (function() {
        var a = document.createElement('a');
        return function(url) {
            a.href = url;
            return a.hostname;
        }
    })();

    var parseEntry = function(el) {
        var date = el.publishedDate || el.pubDate;
        var content = el.content || el.description;
        return { title: el.title, content: content, read: false, date: date, link: el.link, shortLink: hostname(el.link) };
    }

    var parseRSS = function(url, callback) {
        $.ajax({
            url: 'http://ajax.googleapis.com/ajax/services/feed/load?v=1.0&num=50&callback=?&q=' + encodeURIComponent(url),
            dataType: 'json',
            success: callback
        });
    }


    parseRSS($scope.rssFeed, function(data) {
        if (data == null)
            return;
        console.log(data.responseData.feed.entries[0].content);
        $scope.htmltext = data.responseData.feed.entries[0].content;
        var filtered = $scope.htmltext.split('<br>');
        $scope.htmltext = $sce.trustAsHtml(filtered[2]);

        var find = '&quot;';
        var re = new RegExp(find, 'g');
        $scope.htmltext2 = filtered[4].replace(re, '&#39;');
        console.log(filtered);
        $scope.htmltext2 = $sce.trustAsHtml($scope.htmltext2);


        var mostRecentDate = null;
        if ($scope.articles.length && $scope.rssFeed == $scope.originalRssFeed)
            mostRecentDate = $scope.articles[0].date;

        var entries = _.map(data.responseData.feed.entries, function(el) { return parseEntry(el); });

        if (mostRecentDate != null) {
            entries = _.filter(entries, function(el) { return el.date < mostRecentDate; });
        }

        if ($scope.rssFeed != $scope.originalRssFeed) {
            $scope.articles = entries;
            $scope.originalRssFeed = $scope.rssFeed;
        } else
            $scope.articles = _.union($scope.articles, entries);

        $scope.$apply(function() {
            $scope.articles = _.sortBy($scope.articles, function(el) { return el.date; });
        });
    });



}]);

